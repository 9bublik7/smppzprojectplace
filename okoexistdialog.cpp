#include "okoexistdialog.h"
#include "ui_okoexistdialog.h"

OkoExistDialog::OkoExistDialog(QWidget *parent, QString lessonId) :
    QDialog(parent, Qt::Dialog),
    ui(new Ui::OkoExistDialog)
{
    ui->setupUi(this);
    nextDialog=NULL;
    ui->okoListWidget->clear();
    thisLessonId = lessonId;
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr;
    queryStr =  "SELECT study_group "
                "FROM lesson_instance "
                "WHERE lesson_id = '"+lessonId+"' AND data->>'uimo' != ''; ";
    query.prepare(queryStr);
    query.exec();
    int i = 0;
    while(query.next()) {
        ui->okoListWidget->insertItem(i++, query.value(0).toString());
    }
}

OkoExistDialog::~OkoExistDialog()
{
    delete ui;
}

void OkoExistDialog::on_cancelButton_clicked()
{
    this->close();
}

void OkoExistDialog::on_chooseButton_clicked()
{
    const int LESSON_INSTANCE_ID = 0;
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr = "SELECT lesson_instance_id FROM lesson_instance "
                       "WHERE study_group = '"+ui->okoListWidget->currentItem()->text()+"' "
                       "AND lesson_id = '"+ thisLessonId+"';";
    query.prepare(queryStr);
    query.exec();
    query.next();
    if (nextDialog) {
        delete nextDialog;
        nextDialog = NULL;
    }
    nextDialog = new OkoForm(this, query.value(LESSON_INSTANCE_ID).toString());
    nextDialog->exec();
    this->close();
}

void OkoExistDialog::on_okoListWidget_itemSelectionChanged()
{
    ui->chooseButton->setEnabled(true);
}
