#ifndef SPPZSELECT_H
#define SPPZSELECT_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QListWidgetItem>
#include "databasemethods.h"
#include "sppzform.h"
namespace Ui {
class SppzSelect;
}

class SppzSelect : public QDialog
{
    Q_OBJECT

public:
    explicit SppzSelect(QWidget *parent = 0, QString lessonId = "");
    ~SppzSelect();

private slots:
    void on_addButton_clicked();

    void on_changeButton_clicked();

    void on_sppzListWidget_itemClicked(QListWidgetItem *item);

    void on_removeButton_clicked();

private:
    Ui::SppzSelect *ui;
    QDialog* nextWidget;
    QString thisLessonId;
    void fillFormByValues();
protected:
    bool event(QEvent *event);
};

#endif // SPPZSELECT_H
