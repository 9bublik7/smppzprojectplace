#ifndef DATABASEMETHODS_H
#define DATABASEMETHODS_H
#include <QtSql/QSqlDatabase>
#include <QSettings>
#include <QDir>
#include <QCoreApplication>
class DataBaseMethods
{
    static QSqlDatabase db;
    
public:
    DataBaseMethods();
    static bool initDataBase();
    static QSqlDatabase dataBase();
};

#endif // DATABASEMETHODS_H
