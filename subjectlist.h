#ifndef SUBJECTLIST_H
#define SUBJECTLIST_H

#include <QWidget>
#include <QListWidgetItem>
#include <QSqlQuery>
#include "mpczform.h"
#include "mainwindow.h"
#include "databasemethods.h"
namespace Ui {
class SubjectList;
}

class SubjectList : public QWidget
{
    Q_OBJECT

public:
    explicit SubjectList(QWidget *parent = 0);
    ~SubjectList();

    static QString getDisciplineName();
private slots:
    void on_chooseSubjectButton_clicked();

    void on_backButton_clicked();

    void on_subjectListWidget_itemSelectionChanged();

    void on_subjectListWidget_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::SubjectList *ui;
    QWidget* nextWidget;
    QWidget* previousWidget;
    static QString discipline;
    void resizeEvent(QResizeEvent *event);
};

#endif // SUBJECTLIST_H
