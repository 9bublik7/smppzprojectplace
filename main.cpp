#include "mainwindow.h"
#include <QApplication>
#include "databasemethods.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    DataBaseMethods::initDataBase();
    w.show();

    return a.exec();
}
