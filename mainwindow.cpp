#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "subjectlist.h"
#include <QResizeEvent>
#include "utilities.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent, Qt::Dialog),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    nextWidget = NULL;
    Utility::setWidgetOnScreenCenter(this);
    this->setMinimumHeight(this->height());
    this->setMinimumWidth(this->width());
    this->setMaximumHeight(this->height());
    this->setMaximumWidth(this->width());
}

QString MainWindow::accountGroup = "";

void MainWindow::resizeEvent(QResizeEvent *)  {
    Utility::setGroupBoxOnWindowCenter(this, ui->autorizationGroupBox);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_enterInSystemButton_clicked()
{
    // Обработка пустого логина
    if(ui->loginTextBox->text()=="")    {
        QMessageBox::critical(this, "Ошибка", "Введите логин.");
        return;
    }

    // Обработка пустого пароля
    if(ui->passwordTextBox->text()=="")    {
        QMessageBox::critical(this, "Ошибка", "Введите пароль.");
        return;
    }

    // Запрос результата аутентификации у БД
    QSqlQuery query(DataBaseMethods::dataBase());
    bool isAuthenticated = false;
    query.prepare("SELECT (password = crypt('"+ ui->passwordTextBox->text() +"', password)) AS pswmatch , group_of_accounts.name "
                  "FROM account_group_of_accounts_binding "
                  "    INNER JOIN group_of_accounts ON account_group_of_accounts_binding.group_of_accounts_id = group_of_accounts.group_of_accounts_id "
                  "    INNER JOIN account ON account_group_of_accounts_binding.account_id = account.account_id "
                  "WHERE login = '"+ ui->loginTextBox->text() +"';");
    query.exec();
    if(query.next())    {
        isAuthenticated = query.value(0).toBool();
        MainWindow::accountGroup = query.value(1).toString();
    }

    // Проверка по результату
    if(isAuthenticated) {
        if (nextWidget) {
            delete nextWidget;
            nextWidget = NULL;
        }
        nextWidget = new SubjectList(this);
        nextWidget->show();
        this->hide();
    }
    else
        QMessageBox::critical(this, "Ошибка", "Неверный логин или пароль.");
}

QString MainWindow::getAccountGroup(){
    return accountGroup;
}
