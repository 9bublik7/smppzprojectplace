#include "mpczform.h"
#include "ui_mpczform.h"
#include <QDebug>

MPcZForm::MPcZForm(QWidget *parent) :
    QWidget(parent, Qt::Window),
    ui(new Ui::MPcZForm)
{
    ui->setupUi(this);
    ui->MPcZTableWidget->horizontalHeader()->setVisible(true);
    ui->MPcZTableWidget->setColumnWidth(0, 100);
    ui->MPcZTableWidget->setColumnWidth(1, 330);
    this->setMinimumHeight(this->height());
    this->setMinimumWidth(this->width());
    this->setMinimumWidth(this->width());
    this->setMinimumHeight(this->height());
    nextWidget = NULL;
    fillFormByValues();
    setButtonsDisabled();
    setUpSystem();
    ui->MPcZTableWidget->selectRow(0);
    selectedRow = 0;
    if(MainWindow::getAccountGroup()!="administrators")
        ui->settingsButton->setVisible(false);
    if(Setttings::uimmPath.isEmpty())
        Setttings::uimmPath = QDir::homePath();
    if(Setttings::uimoPath.isEmpty())
        Setttings::uimoPath = QDir::homePath();
    if(Setttings::okoPath.isEmpty())
        Setttings::okoPath = QDir::homePath();
    if(Setttings::openEditorCommand.isEmpty())
        Setttings::openEditorCommand = "libreoffice --writer -o ";
}

void MPcZForm::resizeEvent(QResizeEvent *event) {
    // событие при инциалзации не обрабатываем
    if (event->oldSize().width()==-1)
        return;
    int oldWidth = event->oldSize().width();
    int oldHeight = event->oldSize().height();
    int curWidth = event->size().width();
    int curHeight = event->size().height();

    // Расчёт разницы
    int diffWidth = curWidth - oldWidth;
    int diffHeight = curHeight - oldHeight;

    // Изменяем размер групбокса с тейблвиджетом
    ui->MPcZElementsGroupBox->resize(ui->MPcZElementsGroupBox->width() + diffWidth,
                                     ui->MPcZElementsGroupBox->height() + diffHeight);
    ui->MPcZTableWidget->resize(ui->MPcZTableWidget->width() + diffWidth,
                                ui->MPcZTableWidget->height() + diffHeight);
    // Перемещаем кнопки
    ui->addButton->move(ui->addButton->x()+diffWidth, ui->addButton->y());
    ui->backButton->move(ui->backButton->x()+diffWidth, ui->backButton->y());
    ui->changeButton->move(ui->changeButton->x()+diffWidth, ui->changeButton->y());
    ui->okoEditButton->move(ui->okoEditButton->x()+diffWidth, ui->okoEditButton->y());
    ui->removeButton->move(ui->removeButton->x()+diffWidth, ui->removeButton->y());
    ui->sppzEditButton->move(ui->sppzEditButton->x()+diffWidth, ui->sppzEditButton->y());
    ui->uimoEditButton->move(ui->uimoEditButton->x()+diffWidth, ui->uimoEditButton->y());
    ui->uimmEditButton->move(ui->uimmEditButton->x()+diffWidth, ui->uimmEditButton->y());
    ui->settingsButton->move(ui->settingsButton->x()+diffWidth, ui->settingsButton->y());
    ui->MPcZTableWidget->setColumnWidth(1,
                                        ui->MPcZTableWidget->width()-
                                        ui->MPcZTableWidget->columnWidth(0) -
                                        ui->MPcZTableWidget->columnWidth(2) -
                                        ui->MPcZTableWidget->columnWidth(3) -
                                        ui->MPcZTableWidget->columnWidth(4) -
                                        ui->MPcZTableWidget->columnWidth(5) - 17);
}

void MPcZForm::showEvent(QShowEvent *event) {

}
MPcZForm::~MPcZForm()
{
    delete ui;
}

void MPcZForm::on_addButton_clicked()
{
    if (nextWidget) {
        delete nextWidget;
        nextWidget = NULL;
    }
    bool needFilling = false;
    nextWidget = new TPForm(this, needFilling);
    nextWidget->exec();
}

void MPcZForm::on_backButton_clicked()
{
    ((QWidget*)(this->parent()))->show();
    this->close();
}

void MPcZForm::fillFormByValues()   {
    // Сброс таблицы и списка uuid каждого урока
    ui->MPcZTableWidget->setRowCount(0);
    lessonIdList.clear();
    QSqlQuery query(DataBaseMethods::dataBase());
    QString str = " SELECT sequentional_lesson_number, lesson_type, serial_number_of_lesson_type_and_subject, \
            subject_number, subject_name, lesson_number, lesson_name, lesson_id \
            FROM mpcz_information\
            WHERE discipline_name = '"+ SubjectList::getDisciplineName() +"' \
            ORDER BY sequentional_lesson_number ASC;";
    query.prepare(str);
    query.exec();
    for(int i=0; query.next()==true; i++){
        ui->MPcZTableWidget->insertRow(i);
        QTableWidgetItem *item1 = new QTableWidgetItem(query.value(0).toString());
        item1->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
        ui->MPcZTableWidget->setItem(i, 0, item1);
        QString str = query.value(1).toString() + QString(" ") +query.value(2).toString() +
                QString(".\nТема ") + query.value(3).toString() + QString(". ") + query.value(4).toString()+
                QString(".\nЗанятие ") + query.value(5).toString() + QString(". ") + query.value(6).toString();
        QTableWidgetItem *item2 = new QTableWidgetItem(str);
        item2->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
        ui->MPcZTableWidget->setItem(i, 1, item2);
        str = "SELECT data FROM lesson_instance WHERE lesson_id = '"+query.value(7).toString()+"' LIMIT 1;";
        QSqlQuery query1(DataBaseMethods::dataBase());
        query1.prepare(str);
        query1.exec();
        bool isExist[4];
        isExist[0] = false;
        isExist[1] = false;
        isExist[2] = false;
        isExist[3] = false;
        if(query1.next())   {
            QJsonObject obj = QJsonDocument::fromJson(query1.value(0).toByteArray()).object();
            if(obj.contains("scenarialPlan"))
                isExist[0] = true;
            if(obj.contains("uimo"))
                isExist[1] = true;
            if(obj.contains("uimm"))
                isExist[2] = true;
            if(obj.contains("oko"))
                isExist[3] = true;
        }
        for(int j =0; j<4; j++) {
            QString str;
            if(isExist[j]) str = "есть";
            else str = "нет";
            QTableWidgetItem *item = new QTableWidgetItem(str);
            item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
            ui->MPcZTableWidget->setItem(i, j+2, item);
        }
        lessonIdList.insert(i, query.value(7).toString());
    }
    for(int i = 0; i<ui->MPcZTableWidget->rowCount(); i++)  {
        ui->MPcZTableWidget->setRowHeight(i, 60);
    }
}

bool MPcZForm::event(QEvent *event) {
    switch (event->type()) {
    case QEvent::WindowActivate:
        fillFormByValues();
        break;
    default:
        break;
    }
    return QWidget::event(event);
}

void MPcZForm::on_removeButton_clicked()
{
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr = " SELECT lesson_name, lesson_number \
            FROM mpcz_information\
            WHERE sequentional_lesson_number = "+ MPcZForm::getSequentionalLessonNumber() +" ;";
    // Получить наименование выбранного урока
    query.prepare(queryStr);
    query.exec();
    QString lesson_name;
    QString lesson_number;
    QString returnedID;
    if(query.next()){
        lesson_name = query.value(0).toString();
        lesson_number = query.value(1).toString();
    }
    else return;
    // Удалить все LessonInstance
    queryStr = "DELETE FROM lesson_instance WHERE lesson_id = "
                    "(SELECT lesson_id FROM lesson WHERE name = '"+lesson_name+"' and serial_number = "+lesson_number+ ");";
    query.prepare(queryStr);
    query.exec();

    // Удалить занятие
    queryStr = "DELETE FROM lesson \
                 WHERE name = '"+lesson_name+"' and serial_number = "+lesson_number+ " \
                 RETURNING subject_id;";
    query.prepare(queryStr);
    query.exec();
    if(query.next())   {
       returnedID = query.value(0).toString();
    }
    // Ставим активную запись как пустую
    MPcZForm::sln = "";
    lessonIdList.removeAt(ui->MPcZTableWidget->selectedItems().at(0)->row());
    fillFormByValues();
    //Получить список уроков, связанных с id
    queryStr = " SELECT lesson.lesson_id \
            FROM public.lesson\
            WHERE lesson.subject_id = '"+returnedID+"'; ";
    query.prepare(queryStr);
    query.exec();
    if(query.next())    {
        this->show();
        return;
    }
    queryStr="DELETE FROM subject \
        WHERE subject_id = '"+returnedID+"';";
    query.prepare(queryStr);
    query.exec();
    this->update();
}

QString MPcZForm::sln = "";
QString MPcZForm::getSequentionalLessonNumber(){
    return sln;
}

void MPcZForm::on_MPcZTableWidget_cellClicked(int row, int column)
{
}

void MPcZForm::on_MPcZTableWidget_cellDoubleClicked(int row, int column)
{
    if (nextWidget) {
        delete nextWidget;
        nextWidget = NULL;
    }
    bool needFilling = true;
    nextWidget = new TPForm(this, needFilling);
    nextWidget->exec();
    setButtonsDisabled();
}

void MPcZForm::on_changeButton_clicked()
{
    if (nextWidget) {
        delete nextWidget;
        nextWidget = NULL;
    }
    bool needFilling = true;
    nextWidget = new TPForm(this, needFilling);
    nextWidget->exec();
    setButtonsDisabled();
}

void MPcZForm::on_sppzEditButton_clicked()
{
    if (nextWidget) {
        delete nextWidget;
        nextWidget = NULL;
    }
    nextWidget = new SppzSelect(this, lessonIdList.at(selectedRow));
    nextWidget->exec();
    setButtonsDisabled();
}

void MPcZForm::on_uimoEditButton_clicked()
{
    if (nextWidget) {
        delete nextWidget;
        nextWidget = NULL;
    }
    nextWidget = new UimoSelect(this, lessonIdList.at(selectedRow));
    nextWidget->exec();
    setButtonsDisabled();
}

void MPcZForm::on_uimmEditButton_clicked()
{
    if (nextWidget) {
        delete nextWidget;
        nextWidget = NULL;
    }
    nextWidget = new UimmSelect(this, lessonIdList.at(selectedRow));
    nextWidget->exec();
    setButtonsDisabled();
}

void MPcZForm::on_okoEditButton_clicked()
{
    if (nextWidget) {
        delete nextWidget;
        nextWidget = NULL;
    }
    nextWidget = new OkoSelect(this, lessonIdList.at(selectedRow));
    nextWidget->exec();
    setButtonsDisabled();
}

void MPcZForm::setButtonsDisabled() {
    ui->changeButton->setEnabled(false);
    ui->okoEditButton->setEnabled(false);
    ui->removeButton->setEnabled(false);
    ui->sppzEditButton->setEnabled(false);
    ui->uimmEditButton->setEnabled(false);
    ui->uimoEditButton->setEnabled(false);
}

void MPcZForm::on_MPcZTableWidget_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
    if(currentRow!=-1)  {
        QTableWidgetItem* item = ui->MPcZTableWidget->item(currentRow, 0);
        sln = item->text();
        selectedRow = currentRow;
    }
    if(currentRow>=0)   {
        setButtonsEnabled();
    }
    else    {
        setButtonsDisabled();
    }
}

void MPcZForm::on_settingsButton_clicked()
{
    if (nextWidget) {
        delete nextWidget;
        nextWidget = NULL;
    }
    nextWidget = new Setttings(this);
    nextWidget->exec();
    setButtonsDisabled();
}

void MPcZForm::setUpSystem()
{
    QFile file("mpcz_settings.conf");
    bool fileExist = file.exists();
    if(fileExist) {
        QSettings settings("mpcz_settings.conf", QSettings::IniFormat);
        settings.beginGroup("Paths");
        if(settings.contains("uimo"))
            Setttings::uimoPath = settings.value("uimo").toString();
        if(settings.contains("uimm"))
            Setttings::uimmPath = settings.value("uimm").toString();
        if(settings.contains("oko"))
            Setttings::okoPath = settings.value("oko").toString();
        settings.endGroup();
        settings.beginGroup("Programs");
        if(settings.contains("openEditorCommand"))   {
            Setttings::openEditorCommand = settings.value("openEditorCommand").toString();
        }
        settings.endGroup();
    }
    if(Setttings::uimoPath=="" || Setttings::uimmPath=="" || Setttings::okoPath=="")
        Setttings::uimoPath = Setttings::uimmPath = Setttings::okoPath = QDir::currentPath();
}

void MPcZForm::setButtonsEnabled()  {
    ui->changeButton->setEnabled(true);
    ui->okoEditButton->setEnabled(true);
    ui->removeButton->setEnabled(true);
    ui->sppzEditButton->setEnabled(true);
    ui->uimmEditButton->setEnabled(true);
    ui->uimoEditButton->setEnabled(true);
}

QString Setttings::uimmPath = "";
QString Setttings::uimoPath = "";
QString Setttings::okoPath = "";
QString Setttings::openEditorCommand = "libreoffice --writer -o ";
