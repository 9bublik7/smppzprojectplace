#include "uimoselect.h"
#include "ui_uimoselect.h"

UimoSelect::UimoSelect(QWidget *parent, QString lessonId) :
    QDialog(parent, Qt::Dialog),
    ui(new Ui::UimoSelect)
{
    setModal(true);
    ui->setupUi(this);
    thisLessonId = lessonId;
    nextDialog = NULL;
}

void UimoSelect::fillFormByValues() {
    ui->uimoListWidget->clear();
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr;
    queryStr =  "SELECT study_group "
                "FROM lesson_instance "
                "WHERE lesson_id = '"+thisLessonId+"' AND data->>'uimo' != ''";
    query.prepare(queryStr);
    query.exec();
    int i = 0;
    while(query.next()) {
        ui->uimoListWidget->insertItem(i++, query.value(0).toString());
    }
}

void UimoSelect::setButtonsDisabled()
{
    ui->changeButton->setEnabled(false);
    ui->removeButton->setEnabled(false);
}

bool UimoSelect::event(QEvent *event) {
    switch (event->type()) {
    case QEvent::WindowActivate:
        fillFormByValues();
        break;
    default:
        break;
    }
    return QWidget::event(event);
}

UimoSelect::~UimoSelect()
{
    delete ui;
}

void UimoSelect::on_changeButton_clicked()
{
    if (nextDialog) {
        delete nextDialog;
        nextDialog = NULL;
    }
    const int LESSON_INSTANCE_ID = 0;
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr = "SELECT lesson_instance_id FROM lesson_instance "
                       "WHERE study_group = '"+ui->uimoListWidget->currentItem()->text()+"' AND lesson_id = '"+thisLessonId+"';";
    query.prepare(queryStr);
    query.exec();
    query.next();
    nextDialog = new UIMOForm(this, query.value(LESSON_INSTANCE_ID).toString());
    nextDialog->exec();
    setButtonsDisabled();
}

void UimoSelect::on_uimoListWidget_itemClicked(QListWidgetItem *item)
{
    ui->changeButton->setEnabled(true);
    ui->removeButton->setEnabled(true);
}

void UimoSelect::on_addPushButton_clicked()
{
    if (nextDialog) {
        delete nextDialog;
        nextDialog = NULL;
    }
    nextDialog = new UimoExistDialog(this, thisLessonId);
    nextDialog->exec();
    setButtonsDisabled();
}

void UimoSelect::on_removeButton_clicked()
{
    // Получаем поле data в lesson_instance_id
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr = "SELECT data FROM lesson_instance "
                       "WHERE study_group = '"+ui->uimoListWidget->currentItem()->text()+"' AND "
                       "lesson_id = '"+thisLessonId+"';";
    query.prepare(queryStr);
    query.exec();
    if(query.next())    {
        QJsonDocument doc = QJsonDocument::fromJson(query.value(0).toByteArray());
        QJsonObject obj = doc.object();
        obj.remove("uimo");
        if(obj.contains("uimm")) obj.remove("uimm");
        if(obj.contains("oko"))  obj.remove("oko");
        doc.setObject(obj);
        queryStr = "UPDATE lesson_instance "
                   "SET "
                   "data = '"+doc.toJson()+"' "
                   "WHERE study_group = '"+ui->uimoListWidget->currentItem()->text()+"' AND "
                   "lesson_id = '"+thisLessonId+"';";
        query.prepare(queryStr);
        query.exec();
    }
    QMessageBox::information(this, "Сообщение", "УИМ-О для группы "+ ui->uimoListWidget->currentItem()->text() +" удален.");
    setButtonsDisabled();
}
