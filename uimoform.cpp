#include "uimoform.h"
#include "ui_uimoform.h"

UIMOForm::UIMOForm(QWidget *parent, QString lessonInstanceId) :
    QDialog(parent),
    ui(new Ui::UIMOForm)
{
    setModal(true);
    ui->setupUi(this);
    thisLessonInstanceId = lessonInstanceId;
    fillFormByValues();
}

void UIMOForm::showEvent(QShowEvent *)    {
    int questWidth = ui->trainingQuestionTableWidget->width();
    int questItemWidth = ui->trainingQuestionTableWidget->columnWidth(0);
    ui->trainingQuestionTableWidget->setColumnWidth(1, questWidth - questItemWidth-17);
    int litWidth = ui->literatureTableWidget->width();
    int litItemWidth = ui->literatureTableWidget->columnWidth(0);
    ui->literatureTableWidget->setColumnWidth(1, litWidth - litItemWidth-17);
}

void UIMOForm::resizeEvent(QResizeEvent *) {
    int questWidth = ui->trainingQuestionTableWidget->width();
    int questItemWidth = ui->trainingQuestionTableWidget->columnWidth(0);
    ui->trainingQuestionTableWidget->setColumnWidth(1, questWidth - questItemWidth-17);
    int litWidth = ui->literatureTableWidget->width();
    int litItemWidth = ui->literatureTableWidget->columnWidth(0);
    ui->literatureTableWidget->setColumnWidth(1, litWidth - litItemWidth-17);
}

void UIMOForm::fillFormByValues(){
    enum dbObjects {THEME_NUMBER, THEME_NAME,
                    LESSON_TYPE, LESSON_NUMBER, LESSON_NAME,
                    QUESTIONS, LITERATURE,
                    LEARNING_OBJECTIVES, INTRO_TIME, MAIN_TIME, FINAL_TIME, TEST, SIMULATOR,
                    PATH
                   };
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr =  "SELECT "
                        "    subject.serial_number, "
                        "    subject.name, "
                        "    lesson.data -> 'thematicalPlan' ->> 'lessonType' AS lesson_type, "
                        "    lesson.serial_number, "
                        "    lesson.name, "
                        "    lesson.data -> 'thematicalPlan' ->> 'questions' AS questions, "
                        "    lesson.data -> 'thematicalPlan' ->> 'literature' AS literature, "
                        "    lesson_instance.data -> 'scenarialPlan' ->> 'learningObjectives' AS learning_objectives, "
                        "    lesson_instance.data -> 'scenarialPlan' ->> 'introTime' AS intro_time, "
                        "    lesson_instance.data -> 'scenarialPlan' ->> 'mainTime' AS main_time, "
                        "    lesson_instance.data -> 'scenarialPlan' ->> 'finalTime' AS final_time, "
                        "    lesson_instance.data -> 'scenarialPlan' ->> 'test' AS test, "
                        "    lesson_instance.data -> 'scenarialPlan' ->> 'simulator' AS simulator, "
                        "    lesson_instance.data -> 'uimo' ->> 'path' AS path "
                        "FROM "
                        "    lesson_instance "
                        "    INNER JOIN lesson ON lesson_instance.lesson_id = lesson.lesson_id "
                        "    INNER JOIN subject ON lesson.subject_id = subject.subject_id "
                        "WHERE "
                        "   lesson_instance.lesson_instance_id = '"+thisLessonInstanceId+"';";
    query.prepare(queryStr);
    query.exec();
    if (query.next())   {
        ui->themeNumTextBox->setText(query.value(THEME_NUMBER).toString());
        ui->themeNameTextBox->setPlainText(query.value(THEME_NAME).toString());
        ui->classTypeTextBox->setText(query.value(LESSON_TYPE).toString());
        ui->classNumTextBox->setText(query.value(LESSON_NUMBER).toString());
        ui->classNameTextBox->setPlainText(query.value(LESSON_NAME).toString());
        // Ввод question
        QJsonObject jsonObj = QJsonDocument::fromJson(query.value(QUESTIONS).toByteArray()).object();
        QTableWidget* tw = ui->trainingQuestionTableWidget;
        for(int i=0; i<jsonObj.count(); i++){
            tw->insertRow(tw->rowCount());
            QTableWidgetItem *item1 = new QTableWidgetItem;
            item1->setText(QString::number(tw->rowCount()));
            tw->setItem(tw->rowCount()-1, 0, item1);
            item1->setFlags(item1->flags() ^ Qt::ItemIsEditable);
            QTableWidgetItem *item2 = new QTableWidgetItem;
            item2->setText(jsonObj.value(QString::number(tw->rowCount())).toString());
            tw->setItem(tw->rowCount()-1, 1, item2);
            item2->setFlags(item2->flags() ^ Qt::ItemIsEditable);
        }
        // Ввод literature
        jsonObj = QJsonDocument::fromJson(query.value(LITERATURE).toByteArray()).object();
        tw = ui->literatureTableWidget;
        for(int i=0; i<jsonObj.count(); i++){
            tw->insertRow(tw->rowCount());
            QTableWidgetItem *item1 = new QTableWidgetItem;
            item1->setText(QString::number(tw->rowCount()));
            tw->setItem(tw->rowCount()-1, 0, item1);
            item1->setFlags(item1->flags() ^ Qt::ItemIsEditable);
            QTableWidgetItem *item2 = new QTableWidgetItem;
            item2->setText(jsonObj.value(QString::number(tw->rowCount())).toString());
            tw->setItem(tw->rowCount()-1, 1, item2);
            item2->setFlags(item2->flags() ^ Qt::ItemIsEditable);
        }
        ui->learningObjectivesTextBox->setPlainText(query.value(LEARNING_OBJECTIVES).toString());
        // Добавляем вывод времени
        QStringList introTime = query.value(INTRO_TIME).toString().split(':');
        QStringList mainTime  = query.value(MAIN_TIME).toString().split(':');
        QStringList finalTime = query.value(FINAL_TIME).toString().split(':');
        QTime time(0, introTime.at(0).toInt(), introTime.at(1).toInt());
        time = time.addMSecs(QTime(0, mainTime.at(0).toInt(), mainTime.at(1).toInt()).msecsSinceStartOfDay());
        time = time.addMSecs(QTime(0, finalTime.at(0).toInt(), finalTime.at(1).toInt()).msecsSinceStartOfDay());
        int hour = time.hour();
        int min = time.minute();
        int secs = time.second();
        QString timeStr;
        timeStr = timeStr.sprintf("%02d:%02d", min + (hour*60), secs);
        ui->timeTextBox->setText(timeStr);

        ui->testTextBox->setText(query.value(TEST).toString());
        ui->simulatorTextBox->setText(query.value(SIMULATOR).toString());
        ui->filePathTextBox->setText(query.value(PATH).toString());
    }
}

UIMOForm::~UIMOForm()
{
    delete ui;
}

void UIMOForm::on_chancelPushButton_clicked()
{
    this->close();
}

void UIMOForm::on_filePathTextBox_textChanged(const QString &arg1)
{
    if(arg1!="") ui->openPushButton->setEnabled(true);
}

void UIMOForm::on_choosePathPushButton_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(this, "Открыть файл", Setttings::uimoPath, "Файл документов (*.doc *.docx *.odt)", NULL, QFileDialog::DontUseNativeDialog);
    if(filePath.isEmpty() || filePath.isNull())
        return;
    QString &settingPath = Setttings::uimoPath;
    // Проверяем, находимся ли мы в стандартной директории
    if(filePath.indexOf(settingPath)!=0)   {
        // Если нет, то копируем файл.
        QFileInfo fi(filePath);
        QString fileName = fi.fileName();
        QString newFileDir = settingPath + "/" + fileName;
        bool copy = QFile::copy(filePath, newFileDir);
        if(!copy)   {
            int res = QMessageBox::warning(this, "Сообщение", "Такой файл уже существует, вы точной хотите его заменить", QMessageBox::No, QMessageBox::Yes);
            if (res = QMessageBox::Yes) {
                QFile::remove(newFileDir);
                copy = QFile::copy(filePath, newFileDir);
                if(!copy)   {
                    QMessageBox::warning(this, "Сообщение", "У вас недостаточно прав для копирования");
                    return;
                }
                else{
                    QMessageBox::information(this, "Сообщение", "Файл успешно скопирован");
                }
            }
        }
        ui->filePathTextBox->setText(fileName);
    }
    else {
        // Находим относительный путь
        QDir dir(settingPath);
        QString relPath = dir.relativeFilePath(filePath);
        ui->filePathTextBox->setText(relPath);
    }
}

void UIMOForm::on_savePushButton_clicked()
{
    QList<QLineEdit*> lineEditLst = this->findChildren<QLineEdit *>();
    foreach(QLineEdit* item, lineEditLst)   {
        if(item->text().isEmpty())  {
            QMessageBox::warning(this, "Предупреждение", "Не все поля заполнены!!!");
            return;
        }
    }
    QList<QPlainTextEdit*> plainEditLst = this->findChildren<QPlainTextEdit *>();
    foreach(QPlainTextEdit* item, plainEditLst)   {
        if(item->toPlainText().isEmpty())  {
            QMessageBox::warning(this, "Предупреждение", "Не все поля заполнены!!!");
            return;
        }
    }
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr = "SELECT data FROM lesson_instance WHERE lesson_instance_id = '"+thisLessonInstanceId+"';";
    query.prepare(queryStr);
    query.exec();
    if(query.next())    {
        QJsonDocument doc = QJsonDocument::fromJson(query.value(0).toByteArray());
        QJsonObject docObj = doc.object();
        if(docObj.contains("uimo"))   {
            docObj.remove("uimo");
        }
        QJsonObject obj;
        obj.insert("path", ui->filePathTextBox->text());
        docObj.insert("uimo", obj);
        doc.setObject(docObj);
        queryStr = "UPDATE lesson_instance SET data = '"+ doc.toJson()+"' WHERE lesson_instance_id = '"+thisLessonInstanceId+"';";
        query.prepare(queryStr);
        query.exec();
    }
    QMessageBox::information(this, "Сообщение", "УИМ-О сохранен");
    this->close();
}

void UIMOForm::on_openPushButton_clicked()
{
    // Открытие libreoffice writer
    QProcess proc;
    QString path = Setttings::uimoPath.at(Setttings::uimoPath.length()-1)=='/'? Setttings::uimoPath : Setttings::uimoPath + "/";
    proc.start(Setttings::openEditorCommand + " \"" + path + ui->filePathTextBox->text() + "\"");
    while(proc.waitForReadyRead(-1))   {

    }
}

void UIMOForm::on_createNewPushButton_clicked()
{
    bool bOk;
    // Получаем имя файла
    QString name = QInputDialog::getText(this, "Ввод имени файла", "Имя", QLineEdit::Normal, ".odt", &bOk);
    if(!bOk)
        return;
    QStringList nameSplited = name.split(".");
    QString templName = "";
    // Если файл без расширения
    if(nameSplited.length()<=1) {
        QMessageBox::warning(this, "Cообщение", "Укажите тип файла");
        return;
    }
    else    {
        // По типу документа выбираем нужный нам шаблон
        QString docType = nameSplited.at(nameSplited.length()-1);
        if(docType=="odt")
            templName = ":/docs/odtExample.odt";
        if(docType=="doc")
            templName = ":/docs/docExample.doc";
        if(docType=="docx")
            templName = ":/docs/docxExample.docx";
    }
    // Если расширение файло отлично от указанных
    if(templName=="")
        return;
    QString filePath;
    // Добавление слеша, если у пути его нет
    if(Setttings::uimoPath.at(Setttings::uimoPath.length()-1) != QChar('/'))
        filePath = Setttings::uimoPath + "/" + name;
    else
        filePath = Setttings::uimoPath + name;

    // Заменяем файл
    if(!QFile::copy(templName, filePath)){
        int res = QMessageBox::warning(this, "Сообщение", "Такой файл уже существует, вы точно хотите его перезаписать?", QMessageBox::Yes, QMessageBox::No);
        switch (res)    {
            case QMessageBox::Yes:
                QFile::remove(filePath);
                if(!QFile::copy(templName, filePath)) {
                    QMessageBox::warning(this, "Cообщение", "Нет прав на запись в данный каталог");
                    return;
                }
            break;
            case QMessageBox::No:
                return;
        }

    }
    QFile file(filePath);
    bool result = file.setPermissions(QFile::ReadOwner|QFile::WriteOwner);
    // Открытие libreoffice writer
    QProcess proc;
    QString procStr = Setttings::openEditorCommand + " " + "\"" + filePath + "\"";
    proc.start(procStr);
    while(proc.waitForReadyRead(-1))   {

    }

    // Проверка на существование файла
    if(!file.exists()) {
        ui->filePathTextBox->setText("");
        QMessageBox::warning(this, "Информация", "Файл " + name + " не создан");
    }
    else {
        ui->filePathTextBox->setText(name);
        QMessageBox::warning(this, "Информация", "Файл " + name + " создан");
    }
}
