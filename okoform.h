#ifndef OKOFORM_H
#define OKOFORM_H

#include <QDialog>
#include <QSqlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTime>
#include <QFileDialog>
#include <QInputDialog>
#include <QProcess>
#include "databasemethods.h"
#include "setttings.h"
namespace Ui {
class OkoForm;
}

class OkoForm : public QDialog
{
    Q_OBJECT

public:
    explicit OkoForm(QWidget *parent = 0, QString lessonInstanceId = "");
    ~OkoForm();

private slots:

    void on_savePushButton_clicked();

    void on_choosePathPushButton_clicked();

    void on_chancelPushButton_clicked();

    void on_createNewPushButton_clicked();

    void on_openPushButton_clicked();

    void on_filePathTextBox_textChanged(const QString &arg1);

protected:
    void showEvent(QShowEvent *);
    void resizeEvent(QResizeEvent *);
private:
    void fillFormByValues();
    QString thisLessonInstanceId;
    Ui::OkoForm *ui;
};

#endif // OKOFORM_H
