#ifndef UIMOSELECT_H
#define UIMOSELECT_H

#include <QDialog>
#include <QSqlQuery>
#include <QDialog>
#include <QListWidgetItem>
#include <QMessageBox>
#include "databasemethods.h"
#include "uimoform.h"
#include "uimoexistdialog.h"
namespace Ui {
class UimoSelect;
}

class UimoSelect : public QDialog
{
    Q_OBJECT

public:
    explicit UimoSelect(QWidget *parent = 0, QString lessonId = "");
    ~UimoSelect();

private slots:

    void on_changeButton_clicked();

    void on_uimoListWidget_itemClicked(QListWidgetItem *item);

    void on_addPushButton_clicked();

    void on_removeButton_clicked();

private:
    Ui::UimoSelect *ui;
    QString thisLessonId;
    QDialog* nextDialog;
    void fillFormByValues();
    void setButtonsDisabled();
protected:
    bool event(QEvent *event);
};

#endif // UIMOSELECT_H
