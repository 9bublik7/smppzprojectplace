#include "uimoexistdialog.h"
#include "ui_uimoexistdialog.h"

UimoExistDialog::UimoExistDialog(QWidget *parent, QString lessonId) :
    QDialog(parent),
    ui(new Ui::UimoExistDialog)
{
    ui->setupUi(this);
    nextDialog = NULL;
    ui->uimoListWidget->clear();
    thisLessonId = lessonId;
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr;
    queryStr =  "SELECT study_group "
                "FROM lesson_instance "
                "WHERE lesson_id = '"+lessonId+"' AND data->>'scenarialPlan' != ''; ";
    query.prepare(queryStr);
    query.exec();
    int i = 0;
    while(query.next()) {
        ui->uimoListWidget->insertItem(i++, query.value(0).toString());
    }
}

UimoExistDialog::~UimoExistDialog()
{
    delete ui;
}

void UimoExistDialog::on_cancelButton_clicked()
{
    this->close();
}

void UimoExistDialog::on_chooseButton_clicked()
{
    const int LESSON_INSTANCE_ID = 0;
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr = "SELECT lesson_instance_id FROM lesson_instance "
                       "WHERE study_group = '"+ui->uimoListWidget->currentItem()->text()+"' "
                       "AND lesson_id = '"+ thisLessonId+"';";
    query.prepare(queryStr);
    query.exec();
    query.next();
    if (nextDialog) {
        delete nextDialog;
        nextDialog = NULL;
    }
    nextDialog = new UIMOForm(this, query.value(LESSON_INSTANCE_ID).toString());
    nextDialog->exec();
    this->close();
}

void UimoExistDialog::on_uimoListWidget_itemSelectionChanged()
{
    ui->chooseButton->setEnabled(true);
}
