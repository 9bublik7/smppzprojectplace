#-------------------------------------------------
#
# Project created by QtCreator 2017-11-02T09:24:06
#
#-------------------------------------------------

QT       += core gui sql testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CMMPZProject
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    subjectlist.cpp \
    sppzform.cpp \
    uimoform.cpp \
    tpform.cpp \
    uimmform.cpp \
    mpczform.cpp \
    databasemethods.cpp \
    utility.cpp \
    sppzselect.cpp \
    uimmselect.cpp \
    uimoselect.cpp \
    okoselect.cpp \
    okoform.cpp \
    uimoexistdialog.cpp \
    uimmexistdialog.cpp \
    okoexistdialog.cpp \
    setttings.cpp

HEADERS  += mainwindow.h \
    subjectlist.h \
    sppzform.h \
    uimoform.h \
    tpform.h \
    uimmform.h \
    mpczform.h \
    databasemethods.h \
    utility.h \
    sppzselect.h \
    uimmselect.h \
    uimoselect.h \
    okoselect.h \
    okoform.h \
    uimoexistdialog.h \
    uimmexistdialog.h \
    okoexistdialog.h \
    setttings.h

FORMS    += mainwindow.ui \
    subjectlist.ui \
    sppzform.ui \
    uimoform.ui \
    tpform.ui \
    uimmform.ui \
    mpczform.ui \
    sppzselect.ui \
    uimmselect.ui \
    uimoselect.ui \
    okoselect.ui \
    okoform.ui \
    uimoexistdialog.ui \
    uimmexistdialog.ui \
    okoexistdialog.ui \
    setttings.ui

OTHER_FILES +=

RESOURCES += \
    CMMPZResources.qrc
