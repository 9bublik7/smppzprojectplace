#ifndef SPPZSERVERFORM_H
#define SPPZSERVERFORM_H

#include <QWidget>

namespace Ui {
class SPPZServerForm;
}

class SPPZServerForm : public QWidget
{
    Q_OBJECT

public:
    explicit SPPZServerForm(QWidget *parent = 0);
    ~SPPZServerForm();

private:
    Ui::SPPZServerForm *ui;
};

#endif // SPPZSERVERFORM_H
