#ifndef TPFORM_H
#define TPFORM_H

#include <QWidget>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QTableWidget>
#include <QSqlQuery>
#include <QSqlError>
#include <QMessageBox>
#include <QIntValidator>
#include "databasemethods.h"
#include "subjectlist.h"
#include "utility.h"

namespace Ui {
class TPForm;
}

class TPForm : public QDialog
{
    Q_OBJECT

public:
    explicit TPForm(QWidget *parent = 0, bool needFilling = false);
    ~TPForm();

private slots:
    void on_addQuestionButton_clicked();

    void on_addLiteratureButton_clicked();

    void on_deleteQuestionButton_clicked();

    void on_deleteLiteratureButton_clicked();

    void on_saveButton_clicked();


    void on_cancelButton_clicked();
protected:
    void resizeEvent(QResizeEvent *event);
    void showEvent(QShowEvent *);
private:
    Ui::TPForm *ui;
    QWidget* nextWidget;
    QJsonObject getTableWidgetJsonObject(QTableWidget &table);
    QJsonObject getThematicalPlan();
    void fillFormByValues();
    bool saveAtDataBase(QJsonDocument);
    QMessageBox mb;
};

#endif // TPFORM_H
