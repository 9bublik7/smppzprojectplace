#ifndef UIMMSELECT_H
#define UIMMSELECT_H

#include <QDialog>
#include <QSqlQuery>
#include <QListWidgetItem>
#include <QMessageBox>
#include "databasemethods.h"
#include "uimmform.h"
#include "uimmexistdialog.h"

namespace Ui {
class UimmSelect;
}

class UimmSelect : public QDialog
{
    Q_OBJECT

public:
    explicit UimmSelect(QWidget *parent = 0, QString lessonId = "");
    ~UimmSelect();

private slots:
    void on_changeButton_clicked();

    void on_uimmListWidget_itemClicked(QListWidgetItem *item);

    void on_addPushButton_clicked();

    void on_removeButton_clicked();

private:
    Ui::UimmSelect *ui;
    QString thisLessonId;
    QDialog* nextDialog;
    void fillFormByValues();
    void setButtonsDisabled();
protected:
    bool event(QEvent *event);
};

#endif // UIMMSELECT_H
