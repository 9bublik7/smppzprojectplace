#ifndef SETTTINGS_H
#define SETTTINGS_H

#include <QDialog>
#include <QFileDialog>
#include <QSettings>
#include <QMessageBox>
#include <QPlainTextEdit>
#include <QTableWidget>
namespace Ui {
class Setttings;
}

class Setttings : public QDialog
{
    Q_OBJECT

public:
    explicit Setttings(QWidget *parent = 0);
    ~Setttings();
    static QString uimmPath;
    static QString uimoPath;
    static QString okoPath;
    static QString openEditorCommand;

private slots:
    void on_uimmChooseButton_clicked();

    void on_uimoChooseButton_clicked();

    void on_okoChooseButton_clicked();

    void on_cancelButton_clicked();

    void on_saveButton_clicked();

private:
    Ui::Setttings *ui;
};

#endif // SETTTINGS_H
