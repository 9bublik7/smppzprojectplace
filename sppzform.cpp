#include "sppzform.h"
#include "ui_sppzform.h"

SPPZForm::SPPZForm(QWidget *parent, QString lessonId, QString lessonInstanceId) :
    QDialog(parent),
    ui(new Ui::SPPZForm)
{
    setModal(true);
    ui->setupUi(this);
    thisLessonId = lessonId;
    ui->subjectNameTextBox->setText(SubjectList::getDisciplineName());
    thisLessonInstanceId = lessonInstanceId;
    ui->questionTableWidget->resize(484, 70);
    ui->literatureTableWidget->resize(484, 70);
    fillFormByValues();
}

SPPZForm::~SPPZForm()
{
    delete ui;
}

void SPPZForm::showEvent(QShowEvent * )   {
    int questWidth = ui->questionTableWidget->width();
    int questItemWidth = ui->questionTableWidget->columnWidth(1);
    ui->questionTableWidget->setColumnWidth(0, questWidth - questItemWidth-17);
    int litWidth = ui->literatureTableWidget->width();
    int litItemWidth = ui->literatureTableWidget->columnWidth(0);
    ui->literatureTableWidget->setColumnWidth(1, litWidth - litItemWidth-17);
}

void SPPZForm::resizeEvent(QResizeEvent *)  {
    int questWidth = ui->questionTableWidget->width();
    int questItemWidth = ui->questionTableWidget->columnWidth(1);
    ui->questionTableWidget->setColumnWidth(0, questWidth - questItemWidth-17);
    int litWidth = ui->literatureTableWidget->width();
    int litItemWidth = ui->literatureTableWidget->columnWidth(0);
    ui->literatureTableWidget->setColumnWidth(1, litWidth - litItemWidth-17);
}

void SPPZForm::initPlaceComboBox(){
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr;
    queryStr = "SELECT data ->> 'name' as place FROM training_resource;";
    query.prepare(queryStr);
    query.exec();
    while(query.next()) {
        ui->placeComboBox->addItem(query.value(0).toString());
    }
}

void SPPZForm::initTestComboBox(){
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr;
    queryStr = "SELECT data ->> 'name' as testName FROM testing;";
    query.prepare(queryStr);
    query.exec();
    while(query.next()) {
        ui->testComboBox->addItem(query.value(0).toString());
    }
    ui->testComboBox->addItem("Нет");
    ui->testComboBox->setCurrentText("Нет");
}

void SPPZForm::initSimulatorComboBox(){
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr;
    queryStr = "SELECT name FROM simulator;";
    query.prepare(queryStr);
    query.exec();
    while(query.next()) {
        ui->simulatorComboBox->addItem(query.value(0).toString());
    }
    ui->simulatorComboBox->addItem("Нет");
    ui->simulatorComboBox->setCurrentText("Нет");
}

void SPPZForm::fillFormByValues() {
    if (MPcZForm::getSequentionalLessonNumber()=="") return;
    QSqlQuery query(DataBaseMethods::dataBase());
    // получаем наименование и номер урока (чтобы вытащить все остальные данные)
    query.prepare("SELECT lesson_name, lesson_number\
                  FROM mpcz_information\
                  WHERE sequentional_lesson_number = "+MPcZForm::getSequentionalLessonNumber()+";");
    query.exec();
    QString lesson_name, lesson_number, subject_name, subject_number;
    QJsonDocument lesson_data;
    if(query.next()){
        lesson_name = query.value(0).toString();
        lesson_number = query.value(1).toString();
        // Получаем остальные данные для формы
        QString queryStr = "SELECT subject.serial_number, subject.name, lesson.data \
                FROM subject inner join lesson on subject.subject_id = lesson.subject_id \
                WHERE lesson.name = '"+ lesson_name +"' and lesson.serial_number = "+lesson_number+"";
        query.prepare(queryStr);
        query.exec();
        if(query.next())    {
            subject_number = query.value(0).toString();
            subject_name = query.value(1).toString();
            lesson_data = QJsonDocument::fromJson(query.value(2).toByteArray());
        }
        // Начинаем заполнение
        ui->themeNumTextBox->setText(subject_number);
        ui->themeNameTextBox->setPlainText(subject_name);
        ui->classNumTextBox->setText(lesson_number);
        ui->classNameTextBox->setPlainText(lesson_name);
        QJsonObject jsonObject;
        QJsonObject &thematicalPlan = jsonObject;
        thematicalPlan = lesson_data.object()["thematicalPlan"].toObject();
        if(thematicalPlan.contains("lessonType"))
            ui->classTypeTextBox->setText(thematicalPlan.value("lessonType").toString());
        if(thematicalPlan.contains("lessonType"))
            ui->classTypeTextBox->setText(thematicalPlan.value("lessonType").toString());
        if(thematicalPlan.contains("hoursNumber")) {
            ui->numOfHoursTextBox->setText(thematicalPlan.value("hoursNumber").toString());
        }
        if(thematicalPlan.contains("equipment"))
            ui->equipmentTextBox->setPlainText(thematicalPlan.value("equipment").toString());
        if(thematicalPlan.contains("questions")){
            QJsonObject t = thematicalPlan.value("questions").toObject();
            for(int i=0; i<t.count(); i++){
                QTableWidget* tw = ui->questionTableWidget;
                tw->insertRow(tw->rowCount());
                QTableWidgetItem *item1 = new QTableWidgetItem;
                item1->setFlags(item1->flags() ^ Qt::ItemIsEditable);
                item1->setText(t.value(QString::number(tw->rowCount())).toString());
                tw->setItem(tw->rowCount()-1, 0, item1);
                if(thisLessonInstanceId == "")  {
                    QTableWidgetItem *item = new QTableWidgetItem;
                    item->setText("00:00");
                    tw->setItem(tw->rowCount()-1, 1, item);
                }
            }
        }
        if(thematicalPlan.contains("literature"))   {
            Utility::setValuesAtTable(thematicalPlan, "literature", ui->literatureTableWidget);
            for(int i=0; i<ui->literatureTableWidget->rowCount(); i++)  {
                QTableWidgetItem *item = ui->literatureTableWidget->item(i, 1);
                item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            }
        }

        initPlaceComboBox();
        initTestComboBox();
        initSimulatorComboBox();

        QJsonObject &scenarialPlan = jsonObject;
        queryStr = "SELECT lesson_instance.study_group, training_resource.data->>'name' AS place, lesson_instance.data "
                   "FROM lesson_instance INNER JOIN training_resource ON lesson_instance.training_resource_id = training_resource.training_resource_id "
                   "WHERE lesson_instance.lesson_instance_id = '"+ thisLessonInstanceId +"' ;";
        query.prepare(queryStr);
        query.exec();

        if (query.next()) {
            ui->groupTextBox->setText(query.value(0).toString());
            ui->placeComboBox->setCurrentText(query.value(1).toString());
            lesson_data = QJsonDocument::fromJson(query.value(2).toByteArray());
            scenarialPlan = lesson_data.object()["scenarialPlan"].toObject();
            if(scenarialPlan.contains("questionTime"))  {
                for(int i=0; i<ui->questionTableWidget->rowCount(); i++){
                    QTableWidgetItem *item = new QTableWidgetItem;
                    item->setText(scenarialPlan.value("questionTime").toObject().value(QString::number(i+1)).toString());
                    ui->questionTableWidget->setItem(i, 1, item);
                }
            }
            if(scenarialPlan.contains("group"))
                ui->groupTextBox->setText(scenarialPlan.value("group").toString());

            if(scenarialPlan.contains("learningObjectives"))
                ui->learningObjectivesTextBox->setPlainText(scenarialPlan.value("learningObjectives").toString());

            if(scenarialPlan.contains("educationalObjectives"))
                ui->educationalObjectivesTextBox->setPlainText(scenarialPlan.value("educationalObjectives").toString());

            if(scenarialPlan.contains("introTime"))
                ui->introTimeTextBox->setText(scenarialPlan.value("introTime").toString());

            if(scenarialPlan.contains("mainTime"))
                ui->mainTimeTextBox->setText(scenarialPlan.value("mainTime").toString());

            if(scenarialPlan.contains("finalTime"))
                ui->finalTimeTextBox->setText(scenarialPlan.value("finalTime").toString());

            if(scenarialPlan.contains("test"))
                ui->testComboBox->setCurrentText(scenarialPlan.value("test").toString());

            if(scenarialPlan.contains("simulator"))
                ui->simulatorComboBox->setCurrentText(scenarialPlan.value("simulator").toString());;

            if(scenarialPlan.contains("introContent"))
                ui->introContentTextBox->setPlainText(scenarialPlan.value("introContent").toString());

            if(scenarialPlan.contains("mainContent"))
                ui->mainContentTextBox->setPlainText(scenarialPlan.value("mainContent").toString());

            if(scenarialPlan.contains("finalContent"))
                ui->finalContentTextBox->setPlainText(scenarialPlan.value("finalContent").toString());
        }
    }
}

void SPPZForm::on_savePushButton_clicked()
{
    QList<QLineEdit*> lineEditLst = this->findChildren<QLineEdit *>();
    foreach(QLineEdit* item, lineEditLst)   {
        if(item->text().isEmpty())  {
            QMessageBox::warning(this, "Предупреждение", "Не все поля заполнены!!!");
            return;
        }
    }
    QList<QPlainTextEdit*> plainEditLst = this->findChildren<QPlainTextEdit *>();
    foreach(QPlainTextEdit* item, plainEditLst)   {
        if(item->toPlainText().isEmpty())  {
            QMessageBox::warning(this, "Предупреждение", "Не все поля заполнены!!!");
            return;
        }
    }
    QList<QTableWidget*> tableWidgetLst = this->findChildren<QTableWidget *>();
    foreach(QTableWidget* item, tableWidgetLst)   {
        if(item->rowCount() == 0)  {
            QMessageBox::warning(this, "Предупреждение", "Не все поля заполнены!!!");
            return;
        }
    }
    // Получаем training_resource_id
    // Добавляем дополнительную информацию
    QJsonObject lessonInfo;
    QJsonObject questionTime;
    QTableWidgetItem* item;
    for(int i=0; i<ui->questionTableWidget->rowCount(); i++)    {
        item = ui->questionTableWidget->item(i, 1);
        if(item->text().trimmed()!="")
            questionTime.insert(QString::number(i+1), QJsonValue(item->text().trimmed()));
    }

    lessonInfo.insert("questionTime",           QJsonValue(questionTime));
    lessonInfo.insert("learningObjectives",     QJsonValue(ui->learningObjectivesTextBox->toPlainText()));
    lessonInfo.insert("educationalObjectives",  QJsonValue(ui->educationalObjectivesTextBox->toPlainText()));
    lessonInfo.insert("introTime",              QJsonValue(ui->introTimeTextBox->displayText()));
    lessonInfo.insert("mainTime",               QJsonValue(ui->mainTimeTextBox->displayText()));
    lessonInfo.insert("finalTime",              QJsonValue(ui->finalTimeTextBox->displayText()));
    lessonInfo.insert("test",                   QJsonValue(ui->testComboBox->currentText()));
    lessonInfo.insert("simulator",              QJsonValue(ui->simulatorComboBox->currentText()));
    lessonInfo.insert("introContent",           QJsonValue(ui->introContentTextBox->toPlainText()));
    lessonInfo.insert("mainContent",            QJsonValue(ui->mainContentTextBox->toPlainText()));
    lessonInfo.insert("finalContent",           QJsonValue(ui->finalContentTextBox->toPlainText()));
    QJsonObject scenarialPlan;
    scenarialPlan.insert("scenarialPlan", QJsonValue(lessonInfo));
    QJsonDocument doc(scenarialPlan);
    
    // Сохраняем все это
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr;
    QString trainingResourceId;
    queryStr = "SELECT training_resource_id FROM training_resource "
               "WHERE data ->> 'name' = '"+ui->placeComboBox->currentText()+"';";
    query.prepare(queryStr);
    query.exec();
    query.next();
    trainingResourceId = query.value(0).toString();

    // Если вставка новой записи
    if (thisLessonInstanceId=="") {
        queryStr = "INSERT INTO lesson_instance(lesson_id, training_resource_id, study_group, data) VALUES \
                    ('"+ thisLessonId +"', '"+trainingResourceId+"', '"+ ui->groupTextBox->text() +"', '"+ doc.toJson() +"');";
        query.prepare(queryStr);
        query.exec();
    }
    else{
        queryStr = "UPDATE lesson_instance SET "
                   "training_resource_id = '"+trainingResourceId+"', "
                   "study_group = '"+ ui->groupTextBox->text() +"', "
                   "data = '"+ doc.toJson() +"' "
                   "WHERE lesson_instance_id = '"+ thisLessonInstanceId +"';";
        query.prepare(queryStr);
        query.exec();
    }
    QMessageBox::information(this, "Сообщение", "СППЗ сохранен");
    this->close();
}

void SPPZForm::on_cancelPushButton_clicked()
{
    this->close();
}
