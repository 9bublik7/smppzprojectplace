#ifndef MPCZFORM_H
#define MPCZFORM_H

#include <QWidget>
#include <QTableWidget>
#include <QScrollBar>
#include <QResizeEvent>
#include <QSettings>
#include <setttings.h>
#include <tpform.h>
#include <subjectlist.h>
#include "databasemethods.h"
#include "sppzselect.h"
#include "uimmselect.h"
#include "uimoselect.h"
#include "okoselect.h"
#include "mainwindow.h"
namespace Ui {
class MPcZForm;
}

class MPcZForm : public QWidget
{
    Q_OBJECT

public:
    explicit MPcZForm(QWidget *parent = 0);
    static QString getSequentionalLessonNumber();
    ~MPcZForm();

private slots:
    void on_addButton_clicked();

    void on_backButton_clicked();

    void on_removeButton_clicked();

    void on_MPcZTableWidget_cellClicked(int row, int column);

    void on_MPcZTableWidget_cellDoubleClicked(int row, int column);

    void on_changeButton_clicked();

    void on_sppzEditButton_clicked();

    void on_uimoEditButton_clicked();

    void on_uimmEditButton_clicked();

    void on_okoEditButton_clicked();

    void on_MPcZTableWidget_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

    void on_settingsButton_clicked();

private:
    void setUpSystem();
    void showEvent(QShowEvent *event);
    void resizeEvent(QResizeEvent *event);
    void fillFormByValues();
    void setButtonsDisabled();
    void setButtonsEnabled();
    bool event(QEvent *event);
    int selectedRow;
    QList<QString> lessonIdList;
    static QString sln;
    QDialog* nextWidget;
    Ui::MPcZForm *ui;
};

#endif // MPCZELEMENTS_H
