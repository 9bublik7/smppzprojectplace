#ifndef UTILITIES_H
#define UTILITIES_H

#include <QSize>
#include <QPoint>

class Utilities
{
public:
    Utilities();
    static QPoint* getSizeDiffrence(QSize oldSize, QSize newSize);
};

#endif // UTILITIES_H
