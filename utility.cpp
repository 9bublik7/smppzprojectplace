#include "utility.h"

Utility::Utility()
{

}

void Utility::setValuesAtTable(QJsonObject obj, QString objName, QTableWidget* tableWidget){
    QJsonObject t = obj.value(objName).toObject();
    for(int i=0; i<t.count(); i++){
        QTableWidget* tw = tableWidget;
        tw->insertRow(tw->rowCount());
        QTableWidgetItem *item1 = new QTableWidgetItem;
        item1->setText(QString::number(tw->rowCount()));
        tw->setItem(tw->rowCount()-1, 0, item1);
        item1->setFlags(item1->flags() ^ Qt::ItemIsEditable);
        QTableWidgetItem *item2 = new QTableWidgetItem;
        item2->setText(t.value(QString::number(tw->rowCount())).toString());
        tw->setItem(tw->rowCount()-1, 1, item2);
    }
}

void Utility::setWidgetOnScreenCenter(QWidget *widget)  {
    // Получаем ширину и длину экрана
    int desktopWidth = QApplication::desktop()->width();
    int desktopHeight = QApplication::desktop()->height();
    // Получаем ширину и длину виджета
    int widgetWidth = widget->width();
    int widgetHeight = widget->height();
    // Ставим виджет по центру
    widget->move((desktopWidth - widgetWidth)/2, (desktopHeight - widgetHeight)/2);
}

void Utility::setGroupBoxOnWindowCenter(QWidget *widget, QGroupBox *groupBox) {
    // Получаем координаты окна
    int widgetXPos = widget->x();
    int widgetYPos = widget->y();
    // Получаем размеры окна, деленные на 2
    int widgetWidth = widget->width();
    int widgetHeight = widget->height();
    // Получаем размеры групбокса, деленные на 2
    int gbWidth = groupBox->width();
    int gbHeight = groupBox->height();
    // Ставим по центру окна
    groupBox->move((widgetWidth - gbWidth)/2, (widgetHeight - gbHeight)/2);
}
