#include "databasemethods.h"
#include "QMessageBox"


QSqlDatabase DataBaseMethods::db;

bool DataBaseMethods::initDataBase(){
    DataBaseMethods::db = QSqlDatabase::addDatabase("QPSQL");
    // получаем параметры
    QSettings settings(QCoreApplication::applicationDirPath() + "/connection.conf", QSettings::IniFormat);
    settings.beginGroup("Connection");
    QString host = settings.value("host").toString();
    db.setHostName(host);
    settings.endGroup();
    db.setDatabaseName("ts_storage");
    db.setUserName("ts_user");
    db.setPassword("12345678");
    return db.open();
}

QSqlDatabase DataBaseMethods::dataBase() {
    return db;
}
