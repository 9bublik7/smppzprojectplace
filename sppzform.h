#ifndef SPPZFORM_H
#define SPPZFORM_H

#include <QDialog>
#include <QJsonDocument>
#include "subjectlist.h"
#include "utility.h"

namespace Ui {
class SPPZForm;
}

class SPPZForm : public QDialog
{
    Q_OBJECT

public:
    explicit SPPZForm(QWidget *parent = 0, QString lessonId = "", QString lessonInstanceId = "");
    ~SPPZForm();

private slots:
    void on_savePushButton_clicked();

    void on_cancelPushButton_clicked();

protected:
    void resizeEvent(QResizeEvent *);
    void showEvent(QShowEvent *);
private:
    Ui::SPPZForm *ui;
    void fillFormByValues();
    void initPlaceComboBox();
    void initTestComboBox();
    void initSimulatorComboBox();
    QString thisLessonId;
    QString thisLessonInstanceId;
};

#endif // SPPZFORM_H
