#include "okoselect.h"
#include "ui_okoselect.h"

OkoSelect::OkoSelect(QWidget *parent, QString lessonId) :
    QDialog(parent, Qt::Dialog),
    ui(new Ui::OkoSelect)
{
    setModal(true);
    ui->setupUi(this);
    thisLessonId = lessonId;
    nextDialog=NULL;
}

void OkoSelect::fillFormByValues() {
    ui->okoListWidget->clear();
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr;
    queryStr =  "SELECT study_group "
                "FROM lesson_instance "
                "WHERE lesson_id = '"+thisLessonId+"' AND data->>'oko' != '';";
    query.prepare(queryStr);
    query.exec();
    int i = 0;
    while(query.next()) {
        ui->okoListWidget->insertItem(i++, query.value(0).toString());
    }
}

void OkoSelect::setButtonsDisabled()
{
    ui->changeButton->setEnabled(false);
    ui->removeButton->setEnabled(false);
}

bool OkoSelect::event(QEvent *event) {
    switch (event->type()) {
    case QEvent::WindowActivate:
        fillFormByValues();
        break;
    default:
        break;
    }
    return QWidget::event(event);
}

OkoSelect::~OkoSelect()
{
    delete ui;
}

void OkoSelect::on_changeButton_clicked()
{
    const int LESSON_INSTANCE_ID = 0;
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr = "SELECT lesson_instance_id FROM lesson_instance "
                       "WHERE study_group = '"+ui->okoListWidget->currentItem()->text()+"' AND lesson_id = '"+thisLessonId+"';";
    query.prepare(queryStr);
    query.exec();
    query.next();
    if (nextDialog) {
        delete nextDialog;
        nextDialog = NULL;
    }
    nextDialog = new OkoForm(this, query.value(LESSON_INSTANCE_ID).toString());
    nextDialog->exec();
    setButtonsDisabled();
}

void OkoSelect::on_okoListWidget_itemClicked(QListWidgetItem *item)
{
    ui->changeButton->setEnabled(true);
    ui->removeButton->setEnabled(true);
}

void OkoSelect::on_addPushButton_clicked()
{
    if (nextDialog) {
        delete nextDialog;
        nextDialog = NULL;
    }
    nextDialog = new OkoExistDialog(this, thisLessonId);
    nextDialog->exec();
    setButtonsDisabled();
}

void OkoSelect::on_removeButton_clicked()
{
    // Получаем поле data в lesson_instance_id
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr = "SELECT data FROM lesson_instance "
                       "WHERE study_group = '"+ui->okoListWidget->currentItem()->text()+"' AND "
                       "lesson_id = '"+thisLessonId+"';";
    query.prepare(queryStr);
    query.exec();
    if(query.next())    {
        QJsonDocument doc = QJsonDocument::fromJson(query.value(0).toByteArray());
        QJsonObject obj = doc.object();
        obj.remove("oko");
        doc.setObject(obj);
        queryStr = "UPDATE lesson_instance "
                   "SET "
                   "data = '"+doc.toJson()+"' "
                   "WHERE study_group = '"+ui->okoListWidget->currentItem()->text()+"' AND "
                   "lesson_id = '"+thisLessonId+"';";
        query.prepare(queryStr);
        query.exec();
    }
    QMessageBox::information(this, "Сообщение", "УИМ-О для группы "+ ui->okoListWidget->currentItem()->text() +" удален.");
    setButtonsDisabled();
}
