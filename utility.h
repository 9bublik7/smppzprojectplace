#ifndef UTILITY_H
#define UTILITY_H

#include <QJsonObject>
#include <QString>
#include <QTableWidget>
#include <QApplication>
#include <QGroupBox>
#include <QDesktopWidget>
class Utility
{
public:
    Utility();
    static void setValuesAtTable(QJsonObject obj, QString objName, QTableWidget* tableWidget);
    static void setWidgetOnScreenCenter(QWidget* widget);
    static void setGroupBoxOnWindowCenter(QWidget* widget, QGroupBox *groupBox);
};
#endif // UTILITY_H
