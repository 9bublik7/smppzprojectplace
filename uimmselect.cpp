#include "uimmselect.h"
#include "ui_uimmselect.h"

UimmSelect::UimmSelect(QWidget *parent, QString lessonId) :
    QDialog(parent, Qt::Dialog),
    ui(new Ui::UimmSelect)
{
    setModal(true);
    ui->setupUi(this);
    thisLessonId = lessonId;
    nextDialog = NULL;
    fillFormByValues();
}

void UimmSelect::fillFormByValues()  {
    ui->uimmListWidget->clear();
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr;
    queryStr =  "SELECT study_group "
                "FROM lesson_instance "
                "WHERE lesson_id = '"+thisLessonId+"' AND data->>'uimm' != ''; ";
    query.prepare(queryStr);
    query.exec();
    int i = 0;
    while(query.next()) {
        ui->uimmListWidget->insertItem(i++, query.value(0).toString());
    }
}

void UimmSelect::setButtonsDisabled()
{
    ui->changeButton->setEnabled(false);
    ui->removeButton->setEnabled(false);
}

bool UimmSelect::event(QEvent *event) {
    switch (event->type()) {
    case QEvent::WindowActivate:
        fillFormByValues();
        break;
    default:
        break;
    }
    return QWidget::event(event);
}

UimmSelect::~UimmSelect()
{
    delete ui;
}

void UimmSelect::on_changeButton_clicked()
{
    const int LESSON_INSTANCE_ID = 0;
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr = "SELECT lesson_instance_id FROM lesson_instance "
                       "WHERE study_group = '"+ui->uimmListWidget->currentItem()->text()+"' AND lesson_id = '"+thisLessonId+"';";
    query.prepare(queryStr);
    query.exec();
    query.next();
    if (nextDialog) {
        delete nextDialog;
        nextDialog = NULL;
    }
    nextDialog = new UIMMForm(this, query.value(LESSON_INSTANCE_ID).toString());
    nextDialog->exec();
    setButtonsDisabled();
}

void UimmSelect::on_uimmListWidget_itemClicked(QListWidgetItem *item)
{
    ui->changeButton->setEnabled(true);
    ui->removeButton->setEnabled(true);
}

void UimmSelect::on_addPushButton_clicked()
{
    if (nextDialog) {
        delete nextDialog;
        nextDialog = NULL;
    }
    nextDialog = new UimmExistDialog(this, thisLessonId);
    nextDialog->exec();
    setButtonsDisabled();
}

void UimmSelect::on_removeButton_clicked()
{
    // Получаем поле data в lesson_instance_id
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr = "SELECT data FROM lesson_instance "
                       "WHERE study_group = '"+ui->uimmListWidget->currentItem()->text()+"' AND "
                       "lesson_id = '"+thisLessonId+"';";
    query.prepare(queryStr);
    query.exec();
    if(query.next())    {
        QJsonDocument doc = QJsonDocument::fromJson(query.value(0).toByteArray());
        QJsonObject obj = doc.object();
        obj.remove("uimm");
        doc.setObject(obj);
        queryStr = "UPDATE lesson_instance "
                   "SET "
                   "data = '"+doc.toJson()+"' "
                   "WHERE study_group = '"+ui->uimmListWidget->currentItem()->text()+"' AND "
                   "lesson_id = '"+thisLessonId+"';";
        query.prepare(queryStr);
        query.exec();
    }
    QMessageBox::information(this, "Сообщение", "УИМ-О для группы "+ ui->uimmListWidget->currentItem()->text() +" удален.");
    setButtonsDisabled();
}
