#include "tpform.h"
#include "ui_tpform.h"

TPForm::TPForm(QWidget *parent, bool needFilling) :
    QDialog(parent),
    ui(new Ui::TPForm)
{
    ui->setupUi(this);
    setModal(true);
    if(needFilling)
        fillFormByValues();
    ui->subjectNameTextBox->setText(SubjectList::getDisciplineName());
    ui->trainingQuestionTableWidget->resize(484, 70);
    ui->literatureTableWidget->resize(484, 70);
    // Установка валидаторов
    ui->themeNumTextBox->setValidator(new QIntValidator());
    ui->classNumTextBox->setValidator(new QIntValidator());
    ui->numOfHoursTextBox->setValidator(new QIntValidator());
    ui->selfPreparationTextBox->setValidator(new QIntValidator());
}

TPForm::~TPForm()
{
    delete ui;
}

void TPForm::showEvent(QShowEvent* event)   {
    if(!event)
        return;
    int questWidth = ui->trainingQuestionTableWidget->width();
    int questItemWidth = ui->trainingQuestionTableWidget->columnWidth(1);
    ui->trainingQuestionTableWidget->setColumnWidth(0, questWidth - questItemWidth-17);
    int litWidth = ui->literatureTableWidget->width();
    int litItemWidth = ui->literatureTableWidget->columnWidth(0);
    ui->literatureTableWidget->setColumnWidth(1, litWidth - litItemWidth-17);
}

void TPForm::resizeEvent(QResizeEvent *){
    int questWidth = ui->trainingQuestionTableWidget->width();
    int questItemWidth = ui->trainingQuestionTableWidget->columnWidth(0);
    ui->trainingQuestionTableWidget->setColumnWidth(1, questWidth - questItemWidth-17);
    int litWidth = ui->literatureTableWidget->width();
    int litItemWidth = ui->literatureTableWidget->columnWidth(0);
    ui->literatureTableWidget->setColumnWidth(1, litWidth - litItemWidth-17);
}

void TPForm::on_addQuestionButton_clicked()
{
    QTableWidget* t = ui->trainingQuestionTableWidget;
    t->insertRow(t->rowCount());
    QTableWidgetItem *item1 = new QTableWidgetItem;
    item1->setText(QString::number(t->rowCount()));
    t->setItem(t->rowCount()-1, 0, item1);
    item1->setFlags(item1->flags() ^ Qt::ItemIsEditable);
    QTableWidgetItem *item2 = new QTableWidgetItem;
    item2->setText("");
    t->setItem(t->rowCount()-1, 1, item2);
}

void TPForm::on_addLiteratureButton_clicked()
{
    QTableWidget* t = ui->literatureTableWidget;
    t->insertRow(t->rowCount());
    QTableWidgetItem *item1 = new QTableWidgetItem;
    item1->setText(QString::number(t->rowCount()));
    t->setItem(t->rowCount()-1, 0, item1);
    item1->setFlags(item1->flags() ^ Qt::ItemIsEditable);
    QTableWidgetItem *item2 = new QTableWidgetItem;
    item2->setText("");
    t->setItem(t->rowCount()-1, 1, item2);
}

void TPForm::on_deleteQuestionButton_clicked()
{
    QTableWidget *t = ui->trainingQuestionTableWidget;
    t->removeRow(t->currentRow());
}

void TPForm::on_deleteLiteratureButton_clicked()
{
    QTableWidget *t = ui->literatureTableWidget;
    t->removeRow(t->currentRow());
}

void TPForm::on_saveButton_clicked()
{
    QList<QLineEdit*> lineEditLst = this->findChildren<QLineEdit *>();
    foreach(QLineEdit* item, lineEditLst)   {
        if(item->objectName()=="selfPreparationTextBox") continue;
        if(item->objectName()=="checkPointSignTextBox") continue;
        if(item->text().isEmpty())  {
            QMessageBox::warning(this, "Предупреждение", "Не все поля заполнены!!!");
            return;
        }
    }
    QList<QPlainTextEdit*> plainEditLst = this->findChildren<QPlainTextEdit *>();
    foreach(QPlainTextEdit* item, plainEditLst)   {
        if(item->toPlainText().isEmpty())  {
            QMessageBox::warning(this, "Предупреждение", "Не все поля заполнены!!!");
            return;
        }
    }
    QList<QTableWidget*> tableWidgetLst = this->findChildren<QTableWidget *>();
    foreach(QTableWidget* item, tableWidgetLst)   {
        if(item->rowCount() == 0)  {
            QMessageBox::warning(this, "Предупреждение", "Не все поля заполнены!!!");
            return;
        }
    }
    QJsonObject lessonInfo;
    lessonInfo.insert("thematicalPlan", getThematicalPlan());
    QJsonDocument doc(lessonInfo);
    saveAtDataBase(doc);
    QMessageBox::information(this, "Сообщение", "ТП сохранен");
    this->close();

}

QJsonObject TPForm::getTableWidgetJsonObject(QTableWidget &table){
    QJsonObject result;
    QTableWidgetItem* item;
    for(int i=0; i<table.rowCount();i++)    {
        item = table.item(i, 1);
        if(item->text().trimmed()!="")
            result.insert(QString::number(i+1), QJsonValue(item->text().trimmed()));
    }
    return result;
}

QJsonObject TPForm::getThematicalPlan() {
    QJsonObject themPlan;
    themPlan.insert("lessonType", QJsonValue(ui->classTypeComboBox->currentText()));
    themPlan.insert("checkpointSign", QJsonValue(ui->checkPointSignTextBox->text()));
    themPlan.insert("hoursNumber", QJsonValue(ui->numOfHoursTextBox->text()));
    themPlan.insert("equipment", QJsonValue(ui->materialSecurityTextBox->toPlainText()));
    themPlan.insert("questions", getTableWidgetJsonObject(*ui->trainingQuestionTableWidget));
    themPlan.insert("literature", getTableWidgetJsonObject(*ui->literatureTableWidget));
    themPlan.insert("selfPrepare", QJsonValue(ui->selfPreparationTextBox->text()));
    return themPlan;
}

// TODO - Использовать вложенные запросы
bool TPForm::saveAtDataBase(QJsonDocument doc)   {
    const int UNIQUE_VIOLATION = 23505;
    QString returnedID;
    QString queryStr;
    QSqlQuery query(DataBaseMethods::dataBase());

    queryStr = "SELECT discipline.discipline_id\
            FROM public.discipline\
            WHERE discipline.name = '"+ ui->subjectNameTextBox->text() +"';";
    query.prepare(queryStr);
    query.exec();
    if(query.next())
        returnedID = query.value(0).toString();

    // Вставка темы в БД
    queryStr = " INSERT INTO public.subject (subject_id, discipline_id, serial_number, name)\
            VALUES(uuid_generate_v4(), '"+ returnedID +"', \
            "+ ui->themeNumTextBox->text() +", '"+ui->themeNameTextBox->toPlainText()+"') \
            RETURNING subject.subject_id;";
    query.prepare(queryStr);
    query.exec();

    // Обработка ошибки повторной вставки в БД
    switch(query.lastError().number()){
        case UNIQUE_VIOLATION:
            // Получаем uid темы
            queryStr = "SELECT subject_id FROM public.subject \
                        WHERE serial_number = "+ ui->themeNumTextBox->text() +" \
                        AND discipline_id = '"+ returnedID+"' \
                        AND name = '"+ ui->themeNameTextBox->toPlainText() +"';";
            query.prepare(queryStr);
            query.exec();
            if(query.next())
                returnedID = query.value(0).toString();

            queryStr = "UPDATE public.subject \
                        SET serial_number = " + ui->themeNumTextBox->text() + ", \
                            name = '"+ui->themeNameTextBox->toPlainText()+"' \
                        WHERE subject_id = '"+returnedID+"';";
            query.prepare(queryStr);
            query.exec();
    }
    if(query.next())
        returnedID = query.value(0).toString();
    queryStr = "INSERT INTO public.lesson (lesson_id, subject_id, serial_number, name, data) \
            VALUES(uuid_generate_v4(), '"+ returnedID +"', "\
            +ui->classNumTextBox->text() + ", '" + ui->classNameTextBox->toPlainText() + "'\
            , '" + doc.toJson() + "') \
            RETURNING lesson.lesson_id;";
    // insert lesson in base
    query.prepare(queryStr);
    query.exec();
    switch(query.lastError().number()){
        case UNIQUE_VIOLATION:
            queryStr = "SELECT lesson_id FROM public.lesson \
                        WHERE serial_number = "+ ui->classNumTextBox->text() +" \
                        AND subject_id = '"+ returnedID+"' \
                        AND name = '"+ ui->classNameTextBox->toPlainText() +"';";
            query.prepare(queryStr);
            query.exec();
            if(query.next())
                returnedID = query.value(0).toString();

            queryStr = "UPDATE public.lesson \
                        SET serial_number = " + ui->classNumTextBox->text() + ", \
                            name = '"+ui->classNameTextBox->toPlainText()+"', \
                            data = '"+doc.toJson()+"' \
                        WHERE lesson_id = '"+returnedID+"';";
            query.prepare(queryStr);
            query.exec();
    }
    return true;
}

void TPForm::fillFormByValues() {
    if (MPcZForm::getSequentionalLessonNumber()=="") return;
    QSqlQuery query(DataBaseMethods::dataBase());

    // получаем наименование и номер урока (чтобы вытащить все остальные данные)
    query.prepare("SELECT lesson_name, lesson_number\
                  FROM mpcz_information\
                  WHERE sequentional_lesson_number = "+MPcZForm::getSequentionalLessonNumber()+";");
    query.exec();
    QString lesson_name, lesson_number, subject_name, subject_number;
    QJsonDocument lesson_data;
    if(query.next()){
        lesson_name = query.value(0).toString();
        lesson_number = query.value(1).toString();
        // Получаем остальные данные для формы
        QString str = "SELECT subject.serial_number, subject.name, lesson.data \
                FROM subject inner join lesson on subject.subject_id = lesson.subject_id \
                WHERE lesson.name = '"+ lesson_name +"' and lesson.serial_number = "+lesson_number+"";
        query.prepare(str);
        query.exec();
        if(query.next())    {
            subject_number = query.value(0).toString();
            subject_name = query.value(1).toString();
            lesson_data = QJsonDocument::fromJson(query.value(2).toByteArray());
        }
        // Начинаем заполнение
        ui->themeNumTextBox->setText(subject_number);
        ui->themeNameTextBox->setPlainText(subject_name);
        ui->classNumTextBox->setText(lesson_number);
        ui->classNameTextBox->setPlainText(lesson_name);
        QJsonObject tp = lesson_data.object()["thematicalPlan"].toObject();
        if(tp.contains("checkpointSign"))
            ui->checkPointSignTextBox->setText(tp.value("checkpointSign").toString());
        if(tp.contains("lessonType"))
            ui->classTypeComboBox->setCurrentText(tp.value("lessonType").toString());
        if(tp.contains("hoursNumber")) {
            ui->numOfHoursTextBox->setText(tp.value("hoursNumber").toString());
        }
        if(tp.contains("equipment"))
            ui->materialSecurityTextBox->setPlainText(tp.value("equipment").toString());
        if(tp.contains("selfPrepare"))
            ui->selfPreparationTextBox->setText(tp.value("selfPrepare").toString());
        if(tp.contains("questions")){
            Utility::setValuesAtTable(tp, "questions", ui->trainingQuestionTableWidget);
        }
        if(tp.contains("literature"))   {
            Utility::setValuesAtTable(tp, "literature", ui->literatureTableWidget);
        }

    }
}

void TPForm::on_cancelButton_clicked()
{
    this->close();
}
