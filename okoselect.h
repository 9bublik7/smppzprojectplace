#ifndef OKOSELECT_H
#define OKOSELECT_H

#include <QDialog>
#include <QSqlQuery>
#include <QListWidgetItem>
#include <QMessageBox>
#include "databasemethods.h"
#include "okoform.h"
#include "okoexistdialog.h"
namespace Ui {
class OkoSelect;
}

class OkoSelect : public QDialog
{
    Q_OBJECT

public:
    explicit OkoSelect(QWidget *parent = 0, QString lessonId = "");
    ~OkoSelect();

private slots:

    void on_changeButton_clicked();

    void on_okoListWidget_itemClicked(QListWidgetItem *item);

    void on_addPushButton_clicked();

    void on_removeButton_clicked();

private:
    void fillFormByValues();
    void setButtonsDisabled();
    Ui::OkoSelect *ui;
    QString thisLessonId;
    QDialog* nextDialog;
protected:
    bool event(QEvent *event);
};

#endif // OKOSELECT_H
