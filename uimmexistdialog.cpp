#include "uimmexistdialog.h"
#include "ui_uimmexistdialog.h"

UimmExistDialog::UimmExistDialog(QWidget *parent, QString lessonId) :
    QDialog(parent, Qt::Dialog),
    ui(new Ui::UimmExistDialog)
{
    ui->setupUi(this);
    ui->uimmListWidget->clear();
    thisLessonId = lessonId;
    nextDialog=NULL;
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr;
    queryStr =  "SELECT study_group "
                "FROM lesson_instance "
                "WHERE lesson_id = '"+lessonId+"' AND data->>'uimo' != ''; ";
    query.prepare(queryStr);
    query.exec();
    int i = 0;
    while(query.next()) {
        ui->uimmListWidget->insertItem(i++, query.value(0).toString());
    }
}

UimmExistDialog::~UimmExistDialog()
{
    delete ui;
}

void UimmExistDialog::on_cancelButton_clicked()
{
    this->close();
}

void UimmExistDialog::on_chooseButton_clicked()
{
    const int LESSON_INSTANCE_ID = 0;
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr = "SELECT lesson_instance_id FROM lesson_instance "
                       "WHERE study_group = '"+ui->uimmListWidget->currentItem()->text()+"' "
                       "AND lesson_id = '"+ thisLessonId+"';";
    query.prepare(queryStr);
    query.exec();
    query.next();
    if (nextDialog) {
        delete nextDialog;
        nextDialog = NULL;
    }
    nextDialog = new UIMMForm(this, query.value(LESSON_INSTANCE_ID).toString());
    nextDialog->exec();
    this->close();
}

void UimmExistDialog::on_uimmListWidget_itemSelectionChanged()
{
    ui->chooseButton->setEnabled(true);
}
