#ifndef OKOEXISTDIALOG_H
#define OKOEXISTDIALOG_H

#include <QDialog>
#include <QSqlQuery>
#include <databasemethods.h>
#include <okoform.h>

namespace Ui {
class OkoExistDialog;
}

class OkoExistDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OkoExistDialog(QWidget *parent = 0, QString lessonId = "");
    ~OkoExistDialog();

private slots:
    void on_cancelButton_clicked();

    void on_chooseButton_clicked();

    void on_okoListWidget_itemSelectionChanged();

private:
    Ui::OkoExistDialog *ui;
    QDialog* nextDialog;
    QString thisLessonId;
};

#endif // OKOEXISTDIALOG_H
