#include "subjectlist.h"
#include "ui_subjectlist.h"

SubjectList::SubjectList(QWidget *parent) :
    QWidget(parent, Qt::Dialog),
    ui(new Ui::SubjectList)
{
    ui->setupUi(this);
    nextWidget = NULL;
    QSqlQuery result = DataBaseMethods::dataBase().exec("SELECT name FROM discipline");
    while (result.next())   {
        ui->subjectListWidget->addItem(result.value(0).toString());
    }
    setMinimumWidth(this->width());
    setMinimumHeight(this->height());
    setMaximumWidth(this->width());
    setMaximumHeight(this->height());
    parent->resize(this->width(), this->height());
    Utility::setWidgetOnScreenCenter(this);
}

SubjectList::~SubjectList()
{
    delete ui;
    parentWidget()->close();
}

void SubjectList::resizeEvent(QResizeEvent *)  {
    Utility::setGroupBoxOnWindowCenter(this, ui->chooseSubjectGroupBox);
}

void SubjectList::on_chooseSubjectButton_clicked()
{
    if (nextWidget) {
        delete nextWidget;
        nextWidget = NULL;
    }
    nextWidget = new MPcZForm(this);
    this->hide();
    nextWidget->show();
}

void SubjectList::on_backButton_clicked()
{
    ((QWidget*)(this->parent()))->show();
    this->close();
}

QString SubjectList::discipline="";

QString SubjectList::getDisciplineName()  {
    return SubjectList::discipline;
}

void SubjectList::on_subjectListWidget_itemSelectionChanged()
{
    ui->chooseSubjectButton->setEnabled(true);
    SubjectList::discipline=ui->subjectListWidget->selectedItems().at(0)->text();
}

void SubjectList::on_subjectListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    SubjectList::discipline=ui->subjectListWidget->selectedItems().at(0)->text();
    if (nextWidget) {
        delete nextWidget;
        nextWidget = NULL;
    }
    nextWidget = new MPcZForm(this);
    this->hide();
    nextWidget->show();
}
