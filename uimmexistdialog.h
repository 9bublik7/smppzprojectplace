#ifndef UIMMEXISTDIALOG_H
#define UIMMEXISTDIALOG_H

#include <QDialog>
#include <QSqlQuery>
#include "uimmform.h"
#include "databasemethods.h"
namespace Ui {
class UimmExistDialog;
}

class UimmExistDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UimmExistDialog(QWidget *parent = 0, QString lessonId = "");
    ~UimmExistDialog();

private slots:
    void on_cancelButton_clicked();

    void on_chooseButton_clicked();

    void on_uimmListWidget_itemSelectionChanged();

private:
    Ui::UimmExistDialog *ui;
    QDialog* nextDialog;
    QString thisLessonId;
};

#endif // UIMMEXISTDIALOG_H
