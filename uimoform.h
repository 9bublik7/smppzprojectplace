#ifndef UIMOFORM_H
#define UIMOFORM_H

#include <QDialog>
#include <QSqlQuery>
#include <QJsonDocument>
#include <QTime>
#include <QFileDialog>
#include <QProcess>
#include <QInputDialog>
#include <setttings.h>
#include <databasemethods.h>
#include <utility.h>
namespace Ui {
class UIMOForm;
}

class UIMOForm : public QDialog
{
    Q_OBJECT

public:
    explicit UIMOForm(QWidget *parent = 0, QString lessonInstanceId = "");
    ~UIMOForm();

private slots:
    void on_chancelPushButton_clicked();

    void on_filePathTextBox_textChanged(const QString &arg1);

    void on_choosePathPushButton_clicked();

    void on_savePushButton_clicked();

    void on_openPushButton_clicked();

    void on_createNewPushButton_clicked();

private:
    void fillFormByValues();
    Ui::UIMOForm *ui;
    QString thisLessonInstanceId;
    void showEvent(QShowEvent* event);
    void resizeEvent(QResizeEvent* event);
};

#endif // UIMOFORM_H
