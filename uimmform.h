#ifndef UIMMFORM_H
#define UIMMFORM_H

#include <QDialog>
#include <QSqlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTime>
#include <QFileDialog>
#include <QProcess>
#include <QInputDialog>
#include "databasemethods.h"
#include "setttings.h"

namespace Ui {
class UIMMForm;
}

class UIMMForm : public QDialog
{
    Q_OBJECT

public:
    explicit UIMMForm(QWidget *parent = 0, QString lessonInstanceId = "");
    ~UIMMForm();

protected:
    void showEvent(QShowEvent *);
    void resizeEvent(QResizeEvent *);
private slots:
    void on_savePushButton_clicked();

    void on_openPushButton_clicked();

    void on_chancelPushButton_clicked();

    void on_createNewPushButton_clicked();

    void on_choosePathPushButton_clicked();

    void on_filePathTextBox_textChanged(const QString &arg1);

private:
    QString thisLessonInstanceId;
    Ui::UIMMForm *ui;
    void fillFormByValues();
};

#endif // UIMMFORM_H
