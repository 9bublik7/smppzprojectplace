#include "sppzselect.h"
#include "ui_sppzselect.h"
#include "QDebug"
SppzSelect::SppzSelect(QWidget *parent, QString lessonId) :
    QDialog(parent, Qt::Dialog),
    ui(new Ui::SppzSelect)
{
    setModal(true);
    ui->setupUi(this);
    thisLessonId = lessonId;
    fillFormByValues();
    nextWidget=NULL;
}

void SppzSelect::fillFormByValues()    {
    ui->sppzListWidget->clear();
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr;
    queryStr = "SELECT study_group FROM lesson_instance "
               "WHERE lesson_id = '"+ thisLessonId +"'";
    query.prepare(queryStr);
    query.exec();
    int i = 0;
    while(query.next()) {
        ui->sppzListWidget->insertItem(i++, query.value(0).toString());
    }
}

bool SppzSelect::event(QEvent *event) {
    switch (event->type()) {
    case QEvent::WindowActivate:
        fillFormByValues();
        break;
    default:
        break;
    }
    return QWidget::event(event);
}

SppzSelect::~SppzSelect()
{
    delete ui;
}

void SppzSelect::on_addButton_clicked()
{
    if (nextWidget) {
        delete nextWidget;
        nextWidget = NULL;
    }
    nextWidget = new SPPZForm(this, thisLessonId);
    nextWidget->exec();
    this->show();
    ui->changeButton->setEnabled(false);
    ui->removeButton->setEnabled(false);
}

void SppzSelect::on_changeButton_clicked()
{
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr = "SELECT lesson_instance_id FROM lesson_instance "
                       "WHERE study_group = '"+ ui->sppzListWidget->currentItem()->text()+"' AND "
                       "lesson_id = '"+thisLessonId+"';";
    QString lessonInstanceId;
    query.prepare(queryStr);
    query.exec();
    query.next();
    lessonInstanceId = query.value(0).toString();
    if (nextWidget) {
        delete nextWidget;
        nextWidget = NULL;
    }
    nextWidget = new SPPZForm(this, thisLessonId, lessonInstanceId);
    nextWidget->exec();  
}

void SppzSelect::on_sppzListWidget_itemClicked(QListWidgetItem *item)
{
    ui->changeButton->setEnabled(true);
    ui->removeButton->setEnabled(true);
}

void SppzSelect::on_removeButton_clicked()
{
    // Получаем поле data в lesson_instance_id
    QSqlQuery query(DataBaseMethods::dataBase());
    QString queryStr =  "DELETE FROM lesson_instance "
                        "WHERE study_group = '"+ui->sppzListWidget->currentItem()->text()+"' AND "
                        "lesson_id = '"+thisLessonId+"';";
    query.prepare(queryStr);
    query.exec();
    QMessageBox::information(this, "Сообщение", "СМПП для группы "+ ui->sppzListWidget->currentItem()->text() +" удален.");
    ui->changeButton->setEnabled(false);
    ui->removeButton->setEnabled(false);
}
