#ifndef UIMOEXISTDIALOG_H
#define UIMOEXISTDIALOG_H

#include <QDialog>
#include <QSqlQuery>
#include <databasemethods.h>
#include <uimoform.h>
namespace Ui {
class UimoExistDialog;
}

class UimoExistDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UimoExistDialog(QWidget *parent = 0, QString lessonId = "");
    ~UimoExistDialog();

private slots:
    void on_cancelButton_clicked();

    void on_chooseButton_clicked();

    void on_uimoListWidget_itemSelectionChanged();

private:
    Ui::UimoExistDialog *ui;
    QDialog* nextDialog;
    QString thisLessonId;
};

#endif // UIMOEXISTDIALOG_H
