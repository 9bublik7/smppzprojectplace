#include "setttings.h"
#include "ui_setttings.h"

Setttings::Setttings(QWidget *parent) :
    QDialog(parent, Qt::Dialog),
    ui(new Ui::Setttings)
{
    ui->setupUi(this);
    ui->uimoPathTextBox->setText(Setttings::uimoPath);
    ui->uimmPathTextBox->setText(Setttings::uimmPath);
    ui->okoPathTextBox->setText(Setttings::okoPath);
    ui->openEditorTextBox->setText(Setttings::openEditorCommand);
}

Setttings::~Setttings()
{
    delete ui;
}

void Setttings::on_uimmChooseButton_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, "Выбор каталога УИММ-Р", QDir::homePath(), QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks|QFileDialog::DontUseNativeDialog);
    if(!path.isNull() && !path.isEmpty())
        ui->uimmPathTextBox->setText(path);
}

void Setttings::on_uimoChooseButton_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, "Выбор каталога УИМ-О", QDir::homePath(), QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks|QFileDialog::DontUseNativeDialog);
    if(!path.isNull() && !path.isEmpty())
        ui->uimoPathTextBox->setText(path);
}

void Setttings::on_okoChooseButton_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, "Выбор каталога ОК-О", QDir::homePath(), QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks|QFileDialog::DontUseNativeDialog);
    if(!path.isNull() && !path.isEmpty())
        ui->okoPathTextBox->setText(path);
}

void Setttings::on_cancelButton_clicked()
{
    this->close();
}

void Setttings::on_saveButton_clicked()
{
    QList<QLineEdit*> lineEditLst = this->findChildren<QLineEdit *>();
    foreach(QLineEdit* item, lineEditLst)   {
        if(item->text().isEmpty())  {
            QMessageBox::warning(this, "Предупреждение", "Не все поля заполнены!!!");
            return;
        }
    }
    // если все поля заполнены
    QSettings settings("mpcz_settings.conf", QSettings::IniFormat);
    settings.beginGroup("Paths");
    settings.setValue("uimo", ui->uimoPathTextBox->text());
    Setttings::uimoPath = ui->uimoPathTextBox->text();
    settings.setValue("uimm", ui->uimmPathTextBox->text());
    Setttings::uimmPath = ui->uimmPathTextBox->text();
    settings.setValue("oko", ui->okoPathTextBox->text());
    Setttings::okoPath = ui->okoPathTextBox->text();
    settings.endGroup();
    settings.beginGroup("Programs");
    settings.setValue("openEditorCommand", ui->openEditorTextBox->text());
    Setttings::openEditorCommand = ui->openEditorTextBox->text();
    settings.endGroup();
    QMessageBox::information(this, "Сообщение", "Настройки сохранены");
    this->close();
}
