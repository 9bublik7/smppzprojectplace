#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStackedWidget>
#include <QMessageBox>
#include <QApplication>
#include <QDesktopWidget>
#include <databasemethods.h>
#include "subjectlist.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    static QString getAccountGroup();
private:
    Ui::MainWindow *ui;
    QWidget* nextWidget;
    static QString accountGroup;
protected:
    void resizeEvent(QResizeEvent *event);
private slots:
    void on_enterInSystemButton_clicked();
};

#endif // MAINWINDOW_H
